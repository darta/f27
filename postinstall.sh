umount /home/groups
systemctl enable NetworkManager-wait-online.service
systemctl stop firewalld.service
systemctl disable firewalld.service
systemctl disable sshd.service
dnf -y install vim gvim mc
sed -i -e s,'SELINUX=enforcing','SELINUX=permissive', /etc/selinux/config
setenforce 0
dnf -y install  krb5-devel krb5-workstation
dnf -y install pam_mount
    authconfig  --enableshadow --enablelocauthorize --enableldap \
            --ldapserver='ldap' --ldapbase='dc=escoladeltreball,dc=org' \
            --enablekrb5 --krb5kdc='kerberos.informatica.escoladeltreball.org' \
            --krb5adminserver='kerberos.informatica.escoladeltreball.org' --krb5realm='INFORMATICA.ESCOLADELTREBALL.ORG' \
            --updateall

cd /tmp
curl ftp://ftp/pub/deploy.sh | bash
dnf -y install gpm
systemctl daemon-reload
systemctl enable nfs-secure.service
systemctl restart nfs-secure.service
echo '/dev/sda5    /       ext4    defaults        1 1' > /etc/fstab
echo '/dev/sda7  swap      swap    defaults        0 0' >> /etc/fstab
echo 'gandhi:/groups/ /home/groups  nfs4  sec=krb5,comment=systemd.automount  0  0' >> /etc/fstab
sleep 2
echo 'Mounting Ghandi'
mount -a
echo 'exclude=kernel-4*,kernel-core,kernel-modules' >> /etc/dnf/dnf.conf
systemctl enable gpm
systemctl start gpm
dnf -y install pwgen
pwgen -1 | passwd --stdin guest
rpm -ivh http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-27.noarch.rpm
rpm -ivh http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-27.noarch.rpm
dnf -y install http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-adobe-linux
dnf -y install flash-plugin
dnf install chromium vlc gimp git tig gnome-tweak-tool -y

clear
echo 'Gandhi should be mounted:'
mount | grep gandhi
echo ''

echo 'Selinux should be permissive:'
cat /etc/selinux/config | grep permissive
echo ''
echo 'If everything is ok...'
echo '   ...you can reboot now'
